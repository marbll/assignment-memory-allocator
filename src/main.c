#include "mem_internals.h"
#include "mem.h"
#include "util.h"

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

static size_t count_nonfree_blocks(struct block_header* heap_as_block) {
    size_t res = 0;
    while (heap_as_block) {
        if (!heap_as_block->is_free) res++;
        heap_as_block = heap_as_block->next;
    }
    return res;
}

static void test1(struct block_header* heap_as_block) {
    size_t block_size = 500;
    printf("Test 1:\n");
    printf("Allocating a single block\n");

    void* block1 = _malloc(block_size);
    if(block1 == NULL) err("Allocation failed\n");

    struct block_header* block_header = block_get_header(block1);
    if (block_header->is_free) err("Block should not be free\n");
    if (block_header->capacity.bytes != block_size || heap_as_block->capacity.bytes != block_size) err("Block capacity set incorrectly\n");

    _free(block1);
    printf("\nTest 1 passed\n\n");
}

static void test2(struct block_header* heap_as_block) {
    size_t block_size1 = 200;
    size_t block_size2 = 300;

    printf("Test 2:\n");
    printf("Freeing a single block from mutilpe blocks\n");

    void* block1 = _malloc(block_size1);
    if(block1 == NULL) err("Block 1 allocation failed\n");
    void* block2 = _malloc(block_size2);
    if(block2 == NULL) err("Block 2 allocation failed\n");
    _free(block2);

    struct block_header* block_header = block_get_header(block2);
    if (!block_header->is_free) err("Block should be free\n");
    if (count_nonfree_blocks(heap_as_block) != 1) err("Block 2 was not cleared correctly\n");

    _free(block1);
    printf("\nTest 2 passed\n\n");
}

static void test3(struct block_header* heap_as_block) {
    size_t block_size1 = 200;
    size_t block_size2 = 300;
    size_t block_size3 = 400;

    printf("Test 3:\n");
    printf("Freeing two blocks from mutilpe blocks\n");

    void* block1 = _malloc(block_size1);
    if(block1 == NULL) err("Block 1 allocation failed\n");
    void* block2 = _malloc(block_size2);
    if(block2 == NULL) err("Block 2 allocation failed\n");
    void* block3 = _malloc(block_size3);
    if(block3 == NULL) err("Block 3 allocation failed\n");
    _free(block3);
    _free(block2);

    struct block_header* block_header3 = block_get_header(block3);
    if (!block_header3->is_free) err("Block 3 should be free\n");
    struct block_header* block_header2 = block_get_header(block2);
    if (!block_header2->is_free) err("Block 2 should be free\n");
    if (count_nonfree_blocks(heap_as_block) != 1) err("Block 2 or/and 3 were not cleared correctly\n");

    _free(block1);
    printf("\nTest 3 passed\n\n");
}

static void test4(struct block_header* heap_as_block) {
    size_t block_size1 = 10000;

    printf("Test 4:\n");
    printf("Out of memory, extending region\n");

    void* block1 = _malloc(block_size1);
    if(block1 == NULL) err("Block 1 allocation failed\n");

    if (count_nonfree_blocks(heap_as_block) != 1) err("Block extending region was not allocated correctly\n");

    _free(block1);
    printf("\nTest 4 passed\n\n");
}

int main() {
    struct block_header* heap_as_block = (struct block_header*) heap_init(8175);
    if (heap_as_block == NULL) err("Heap initialization failed\n");
    else {
        test1(heap_as_block); // Обычное успешное выделение памяти
        test2(heap_as_block); // Освобождение одного блока из нескольких выделенных
        test3(heap_as_block); // Освобождение двух блоков из нескольких выделенных
        test4(heap_as_block); // Память закончилась, новый регион памяти расширяет старый
        printf("Tests passed\n");
    }
    return 0;
}
